SQLite3TwoFish
===========

SQLite3 encryption extension with TwoFish.
----
Come from https://github.com/utelle/wxsqlite3

TwoFish source code
----
https://www.schneier.com/academic/twofish/download.html

Command
----
    .open sample.dbx
    pragma key='Abc123';
    CREATE TABLE peopleInfo(peopleInfoID integer primary key, firstname text, lastname text, age integer);
    insert into peopleInfo values(null, 'Tom', 'King', 40);
    insert into peopleInfo values(null, 'Peter', 'Trump', 20);
    select * from peopleInfo;
